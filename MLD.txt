Création des tables
--------------------------------------------------------

- Editor(#id: INTEGER , name: str , contact: str , url: str)
    avec    name, contact et url UNIQUE et NOT NULL
           
            (name, contact, url) clé candidate

    id -> name, contact, url
    (name, contact, url) -> id 
    name -> contact, url 
    contact -> name, url
    url -> name, contact 

- Users(#id: INTEGER , name: str, firstname: str , mail: str , password: str , birthdate: date, admin_priviledge: bool, newsletter_subscription: bool)
    avec    mail UNIQUE et NOT NULL,
            name, firstname, password, birthdate, admin_priviledge, newsletter_subscription NOT NULL
            
            (mail est une clé candidate)

    id -> name, firstname, mail, password, birthdate, admin_priviledge, newsletter_subscription
    mail -> id, name, firstname, password, birthdate, admin_priviledge, newsletter_subscription

- Prepaid_card(#serial_number: str , starting_amount: float, current_amount: float , start_date: date, end_date: date, belong_to_user_id=>Users)
    avec    starting_amount, current_amount, start_date, end_date, belong_to_user_id NOT NULL

    serial_number -> starting_amount, current_amount, start_date, end_date, belong_to_user_id


- Credit_card(#serial_number: str , start_date: date, end_date: date , belong_to_user_id=>Users)
    avec    start_date, end_date, belong_to_user_id NOT NULL

    serial_number -> start_date, end_date, belong_to_user_id


- Device_type(#type: str)



- Application(#id: INTEGER , title: str , cost_of_download: float , age_restriction: INTEGER , editor_id=>Editor)
    avec    title, cost_of_download et editor_id NOT NULL
            utilisation d'un id car on peut potentiellement avoir plusieurs applications avec un même titre et un même prix
           
            (title, cost_of_download, editor_id) est une clé candidate 

    id -> title, cost_of_download, age_restriction, editor_id
    (title, cost_of_download, editor_id) -> id, age_restriction


- Ressource(#id: INTEGER , title: str , cost_of_download: float , age_restriction: INTEGER , application_id=>Application , editor_id=>Editor)
    avec    title, application_id et editor_id, cost_of_download NOT NULL
            utilisation d'un id pour la même raison que pour les applications
            
            (title, application_id, editor_id) clé candidate 
            (title, cost_of_download, application_id) est une clé candidate 
            (title, cost_of_download, editor_id) est une clé candidate 

    id -> title, cost_of_download, age_restriction, application_id, editor_id
    (title, application_id, editor_id) -> id, cost_of_download, age_restriction
    (title, cost_of_download, application_id) -> id, age_restriction, editor_id
    (title, cost_of_download, editor_id) -> id, age_restriction, application_id


- Constructor(#name: str, country_origin: str)
    avec    country_origin NOT NULL

    name -> country_origin


- Operating_system(#id: INTEGER, name: str , version: float, constructor_name=>Constructor)
    avec    name, version, constructor_name NOT NULL
            
            (name, version) est une clé candidate

    id -> name, version, constructor_name
    (name, version) -> id, constructor_name


- Device(#serial_number: str , commercial_name: str, type=>Device_type, OS=>Operating_system, constructor_name=>Constructor, user_id=>Users)
    avec    commercial_name, type, OS, constructor_name, user_id NOT NULL
            
            (user_id, commercial_name) est une clé candidate

    serial_number -> commercial_name, type, OS, constructor_name, user_id
    commercial_name -> type, OS, constructor_name
    OS -> type 


- Review(#id: INTEGER , mark: INTEGER, title: str, content: text, application_id=>Application)
    avec    mark NOT NULL et compris entre 1 et 5
            title, content, application_id NOT NULL
            
            (mark, title, content) est une clé candidate
            (content, application_id) peut aussi être considéré une clé candidate

    id -> mark, title, content, application_id
    (mark,title, content) -> application_id, id 
    (content, application_id) -> mark, title, id 




- Subscription(#id: INTEGER, application_id=>Application ,start_date: date, end_date; date, periodicity: PERIODICITY, auto_debit: bool, nb_month_debit: INTEGER, cost: float)
    avec    application_id, start_date, end_date, periodicity, auto_debit, cost NOT NULL
            PERIODICITY est un type ENUM : {monthly, quaterly, yearly}
            si auto_debit est TRUE, alors nb_month_debit est NULL
            si auto_debit est FALSE, alors nb_month_debit n'est pas NULL
            on utilise un id car il peut y avoir plusieurs abonnements pour une seule application, se qui rend l'utilisation de application_id en tant que clé primaire impossible
            
            On peut avoir des abonnements de période différente, avec (ou non) un prélèvements automatique, les dates de début et de fin peuvent aussi être différentes, donc on ne peut pas avoir d'autre clé candidate

    id -> application_id, start_date, end_date, periodicity, auto_debit, nb_month_debit, cost 
    nb_month_debit -> auto_debit



Création des classes d'association
--------------------------------------------------------

- Compatible(#application_id=>Application, #OS=>Operating_system)

- Download_app(#user_id=>Users, #application_id=>Application)

- Download_ressource(#user_id=>Users, #ressource_id=>Ressource)




Normalisation :
--------------------------------------------------------

- tous les attributs sont atomiques, et les tables possèdent au moins une clé, on est donc en 1NF

- l'utilisation d'ID pour identifier les différents tuples dans les tables : 
    - Editor
    - Users
    - Application
    - Ressource
    - Operating_system
    - Review
    - Subscription
    permet de jusitifer la 2NF : chaque attribut non clé dépend obligatoirement de l'ensemble des clés (donc ici de l'ID)

- pour les autres tables, on a des numéros de séries qui ont le même fonctionnement que des id 


Prenons Table par Table : 

- Editor : 
    - j'aurais pu me passer de l'ID dans cette table, en effet, les attributs name, contact, et url étant aussi UNIQUE et NOT NULL, ils sont aussi des clés candidates. On en déduit donc chaque relation de cette table est en 3NF et qu'elle respecte la BCNF. Aucune DFE n'est présente puisque la seule DF est la suivante : 
    id -> name, contact, url 
    De plus, ni le name, ni le contact, ni l'url seulement ne permettent de donner l'id

- Users 
    - l'id nous permet d'avoir tous les attributs grâce à la DF suivante : 
        id -> name, firstname, mail, password, birthdate, admin_priviledge, newsletter_subscription
    - le mail étant une clé candidate (UNIQUE et NOT NULL), on ne le considèrera pas pour prouver la BCNF
    - le couple (name, firstname) ne nous permet pas de deviner un autre attrinut, en effet, on pourrait se retrouver avec 2 personnes ou plus portant le même nom et prénom, sans pour autant avoir le même mot de passe, ou le même admin_priviledge ou encore le fait d'être abonné à la newsletter
    - les attributs newsletter_subscription et admin_priviledge étant des booléans, il est assez évident qu'ils ne sont pas suffisant pour identifier une personne 
    - la date de naissance ne permet pas non plus d'identifier une personne puisque 2 personnes peuvent être né le même jour
    -> chaque relation respecte la BCNF

- Prepaid_card
    - ici le numéro de série fonctionne de la même façon qu'un id puisque c'est son but 
    - aucun des attributs ne permet de déterminer un autre attribut non clé
        par exemple, même si nous prenons 2 cartes achetés le même jour, par la même personne, avec le même montant dessus, et qu'elle utilise les 2 cartes pour acheter quelque chose au même prix, cela ne permet toujours pas de déterminer le numémro de série

- Credit_card
    - le serial_number fonctionne encore comme un id
    - comme un utilisateur peut avoir plusieurs cartes de crédit, l'attribut seul ne permet pas de déterminer le numéro de série

- Device_type
    - pas de DF 
    - relation toute clé

- Application / Ressource
    - la grande majorité des applications et des packs étant gratuit, et sans restriction d'âge, ces 2 attributs ne peuvent pas permettre d'identifier une application ou une ressource, c'est pourquoi ils ne peuvent pas se déterminer entre eux. 
    - l'id permet donc d'identifier l'application ou le ressource, et lui seul permet de déterminer les autres attributs 
    -> nous avons donc bien une relation BCNF

- Constructor   
    - une seule DF et le pays d'origine ne permet en rien d'identifier le constructeur car plusieurs constructeurs peuvent avoir un même pays d'origine
    - on a donc bien une relation qui respècte la BCNF

- Operating_system
    - utilisation d'un id car il peut y avoir un même nom de système mais avec des versions différentes 
    - le numéro de version peut petre partagé par plusieurs OS 
    - le nom de l'OS peut définir plusieurs versions 
    - il n'y a donc pas de dépendance entre les attributs, nous respectons donc bien la BCNF

- Device 
    - le numéro de série fait office d'id 
    - le nom commercial est défini via le numéro de série (qui est la clé primaire)
    - les autres attributs sont des clés étrangères et sont aussi déterminé par le numéro de série, même si d'un point de vue sémantique, il serait possible de définir le nom de l'OS (mais pas la version) ou le nom du constructeur via le nom commercial 

- Review 
    - il est évident que la note ne permet pas d'identifier un commentaire 
    - Le titre et le contenu ne permettent pas non plus de définir d'autre attribut, car par exemple il est possible d'avoir 2 commentaires avec le même titre mais des notes ou un contenu différent 

- Subscription
    - la date de début et la date de fin sont 2 attribbuts indépendants, dans le cadre d'une application, il n'est pas rare d'avoir des abonnements avec une durée commune pour tout le monde 
    - la périodicité ne peut prendre que 3 valeurs dans notre cas, elle ne permet pas donc de déterminer un autre attribut, car il est évident que plusieurs abonnements différents pourraient avoir des périodicités différentes 
    - l'attribut auto_debit étant un booléan il ne peut pas déterminé un autre attribut (dans notre cas et avc nos attributs)
    - les coûts d'un abonnement sont toujours sensiblement les mêmes, ils ne sont donc pas suffisants pour identifier un abonnement ou pour définir un autre attribut : ce n'est pas car un abonnement coûte 9,99€ qu'il est forcément mensuel par exemple
    - en revanche, l'attribut nb_month_debit peut permettre de déterminé l'attribut auto_debit. En effet, si nb_month_debit n'est pas NULL, cela veut forcément dire que auto_debit est TRUE, et inversement, si nb_month_debit est NULL, cela veut dire que auto_debit est TRUE. Il y a donc un non respect de la 3NF ici 

- les  3 classes d'association
    - chaque attribut de ces relations sont des clés, ce sont donc des relations toutes clés