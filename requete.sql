
--- ***** Requêtes sur les VUES ***** ---


-- permet de voir le nombre de terminal enregistré par utilisateur
SELECT * FROM projet.v_user_device;

-- permet de voir toutes les applications (ou ressources) téléchargées par tous les utilisateurs
SELECT * FROM projet.v_user_downloaded_app;
SELECT * FROM projet.v_user_downloaded_ressource;

-- permet d'avoir le nombre de téléchargement de chaque application (ou ressource), ainsi que son profit
SELECT * FROM projet.v_nb_download_app;
SELECT * FROM projet.v_nb_download_ressource;

-- permet d'avoir directement le nom de toutes les applications/ressources développées par un éditeur
SELECT * FROM projet.v_editors_app; 
SELECT * FROM projet.v_editors_ressource; 




--- ***** Requêtes classiques ***** ---

-- permet de voir les avis laissés sur une application (à choisir selon son id)
-- par exemple pour l'application qui porte l'identifiant numéro 1
SELECT * FROM projet.review WHERE application_id = 1;

-- permet de voir tous les terminaux d'un utilisateur (à choisir selon son id)
-- par exemple pour l'utilisateur numéro 16 (= Nathan TERRADE)
SELECT * FROM projet.device WHERE user_id = 16; 

-- permet de voir le nombre total de téléchargement du store (applications + ressources combinées)
-- on pourrait passer par des foncions pour réaliser ce genre de requête afin d'éviter les requêtes imbriquées
SELECT 
(SELECT COUNT(application_id) FROM projet.download_app) AS nb_download_app,
(SELECT COUNT(ressource_id) FROM projet.download_ressource) AS nb_download_ressource,
((SELECT COUNT(application_id) FROM projet.download_app)+(SELECT COUNT(ressource_id) FROM projet.download_ressource)) AS nb_download_total;

-- permet de voir le nombre total d'utilisateurs enregistrés sur le store
SELECT COUNT(*) AS nb_users FROM projet.users;

-- permet de voir le nombre total d'éditeurs enregistrés sur le store
SELECT COUNT(*) AS nb_editors FROM projet.editor;

-- permet de calculer le profit global du store
SELECT SUM(((nb_download*cost_of_download)*0.3)) AS total_profit_with_commission FROM projet.v_nb_download_app;

-- permet de voir le total d'argent dépensé par tous les utilisateurs sur le store 
-- comme pour le nombre de téléchargement, il aurait été préférable de passer par une fonction plutôt que d'utiliser des requêtes imbriquées
SELECT
	(SELECT SUM((nb_download * cost_of_download)) FROM projet.v_nb_download_app) AS total_spent_on_app ,
	(SELECT SUM((nb_download * cost_of_download)) FROM projet.v_nb_download_ressource) AS total_spent_on_ressource,
	((SELECT SUM((nb_download * cost_of_download)) FROM projet.v_nb_download_app) + 
	(SELECT SUM((nb_download * cost_of_download)) FROM projet.v_nb_download_ressource)) AS total_spent;

