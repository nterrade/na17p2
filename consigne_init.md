# Sujet 30 : Plate-forme de téléchargement d'applications en ligne

- On souhaite gérer les données liées à un nouveau service d'achat d'applications mobiles : le Nimpstore.
- Ce service doit permettre à des clients d'acheter des applications, ou des ressources pour ces applications, et de les installer sur leur terminaux mobiles (smartphones, tablettes, montres connectées...). Ce service est complétement indépendant du système d'exploitation et est donc potentiellement disponible sur tous les terminaux mobiles. Une partie communautaire permet aux utilisateurs de donner des avis sur les applications.
- On souhaite tracer toutes les activités sur le Nimpstore pour différentes évaluations (performances de la plateforme, profit pour les éditeurs d'applications, activités des utilisateurs...).

## Hypothèses

- Un terminal est la propriété d'un client unique enregistré sur la plateforme. Il est décrit par son modèle ainsi qu'un numéro de série unique. Un modèle est décrit par un constructeur et par une désignation commerciale. Chaque modèle est doté d'un système d'exploitation. On identifie un système d'exploitation par son constructeur et sa version.
- Il y a différentes formes d'achat :
  - l'achat simple permet de disposer d'une application, ou d'une ressource (c'est à dire du contenu supplémentaire pour une application), pour une durée illimitée sur tous les terminaux. L'application, ou la ressource, a donc un coût fixe (qui peut être nul).
  - l'abonnement permet l'utilisation d'une application pour une période déterminée sur tous les terminaux. L'application peut être téléchargée et installée gratuitement mais nécessite un coût périodique (en mois, trimestres ou années) pour être utilisée. Lors de la souscription d'un abonnement, l'utilisateur peut choisir entre une formule automatique (abonnement reconduit tacitement),ou choisir un nombre de mois.
- Quelle que soit la forme de l'achat, l'application ou la ressource est installable et accessible pour tous les terminaux enregistrés par ce client (limité à 5).
- **Lorsqu'un client achète une application ou une ressource, il peut la destiner à lui-même ou à un autre client.**
- Les paiements peuvent être réalisés par carte bancaire ou par carte prépayée. Les informations relatives à ces paiements sont stockées.
- Les cartes prépayées ont un montant de départ, un montant courant ainsi qu'une date de validité.
- Les applications et les ressources sont proposées sur le Nimpstore par des éditeurs. L'éditeur d'une ressource n'est pas nécessairement le même que l'application à laquelle elle se destine. Un éditeur est décrit par un nom, un contact et une url.
- Les applications et ressources sont identifiées par un titre. Les ressources ne s'appliquent qu'à une seule application.
- Les applications et les ressources ne sont disponibles que pour certaines versions de certains systèmes d'exploitation.
- Un système d'exploitation n'est disponible que pour certains modèles de téléphone.
- Les utilisateurs peuvent donner des avis sur les applications qu'ils ont installés. Pour cela, ils attribuent une note (sur 5) ainsi qu'un commentaire dont la taille est limitée à 500 caractères.

## Besoins

### Nous souhaitons proposer différentes vues.

- **Une vue utilisateur :**

- Connaître les applications compatibles avec ses appareils
- Avoir un historique des achats et des installations
- Gérer ses terminaux (ajout/retrait)

- **Une vue administrateur :**

- Ajouter des applications et des ressources
- Ajouter des éditeurs
- Accorder des cartes prépayées à des clients

- **Une vue analyste :**

- Connaître les applications les plus rentables
- Connaître les éditeurs qui réalisent le plus de profit et le plus de vente (en quantité).
- Connaître le nombre d'installations d'une application/ressources
- Évaluer les profits réalisés par le service distributeur, sachant qu'il prend une part de 30% du prix affiché.
- Évaluer les profits réalisés par le service éditeur, (70% du prix).
- Connaître les utilisateurs les plus actifs (en termes de nombre d'avis rendus
