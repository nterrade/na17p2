# Note de clarification 

Sujet 30 : Plate-forme de téléchargement d'applications en ligne

Nous ne traiteons ici que la partie base de donnée.
Aucun script python ne sera demandé pour se projet.

# 1. Les objets / services

1. le store d'application : NimpStore
    - contient toutes les applications et les ressources
    - est indépendant de l'OS
2. une application
    - possède un titre
    - possède un éditeur
    - possède un coût de téléchargement : gratuit ou payant 
    - peut posséder une ou plusieurs ressources supplémentaires
    - peut nécessiter une restriction d'âge
    - peut nécessiter un abonnement qui :
        - possède une date de fin de validité
        - possède une périodicité :
            - tous les mois
            - tous les trimestres
            - tous les ans
        - peut choisir d'activer le renouvellement automatique du prélèvement OU de choisir un nombre de mois prédéfini
    - compatible uniquement avec certain OS (pas forcément tous)
3. une ressource
    - possède un titre
    - concerne une seule application
    - peut nécessiter une restriction d'âge
4. un avis
    - possède une note (note : entier compris entre 0 et 5) 
    - possède un contenu textuel 
    - possède un titre
    - est lié à une seule application
5. un terminal
    - appartient à un utilisateur unique
    - smartphone, tablette ou montre connectée 
    - possède un constructeur
    - possède une désignation commerciale
    - possède un OS
6. un constructeur de terminaux et/ou de système d'exploitation
    - possède un nom (hypothèse)
    - possède un pays d'origine 
7. un système d'exploitation (=OS)
    - possède un constructeur
    - possède une version
8. un utilisateur
    - possède un nom (hypothèse)
    - possède un prénom (hypothèse)
    - possède une adresse mail (hypothèse)
    - possède au moins un terminal et au plus 5
    - possède un âge (hypothèse)
    - peut télécharger une ou plusieurs application(s)
    - peut télécharger une ou plusieurs ressource(s)
9. un éditeur
    - possède un nom
    - possède un contact sous forme d'adresse mail (hypothèse)
    - possède un URL
    - peut proposer une ou plusieurs application(s)
    - peut proposer une ou plusieurs ressources(s)
10. type de paiement
    - carte bancaire
        - possède un numéro de série
        - possède une date de fin de validité
        - est associée à une seule personne, le nom de la carte est le même que le couple "nom, prénom" du propriétaire de la carte. (hypothèse)
    - carte prépayée
        - possède un montant de départ
        - possède un montant courant
        - possède une date de validité


# 2. Les utillisateurs 

1. utilisateur lambda
2. éditeur (d'une ou plusieurs applications, ou d'une ou plusieurs ressources additionnel sur une application)
3. super-user : admin du store, il a accès aux statistiques de performances de la plateforme


# 3. Les fonctions

## Tous les utilisateurs (lambda et éditeur)
1. peuvent télécharger une application
2. peuvent télécharger une ressource
3. peuvent laisser un avis
4. peuvent savoir quelle(s) application(s) sont compatibles avec ces systèmes d'exploitations
5. consulter ses applications téléchargées
6. ajouter ou supprimer un terminal

## Tous les éditeurs
1. peuvent proposer une application
2. peuvent proposer une ressource 
3. visualiser le nombre de téléchargement pour ses applications
4. visualiser le profit par application (uniquement les siennes), après déduction des 30% pris par le store

## Tous les super-users
1. récupérer à n'importe quel moment :
    - le nombre de téléchargements total
    - le total dépensé sur le store
    - le nombre total d'utilisateur
    - le nombre total d'applications
    - le nombre total de ressources
    - le profit générer par le store avec la commission de 30% prises pour chaque achat effectué sur le store
    - le profit par éditeur
    - le nombre de téléchargements par éditeur
    - le nombre de téléchargement pour une application
    - le nombre de téléchargement par utilisateur
    - le total dépensé par utilisateur
2. ajouter des éditeurs


# 4. Les hypothèses

1. Un utilisateur possède obligatoirement un nom, un prénom, un âge et une adresse mail.
2. Le coût de développement d'une application ou d'une ressource est nul.
3. Le contact d'un éditeur est toujours sous la forme d'une adresse mail.
4. On ne peut pas laisser d'avis sur une ressource.
5. Les ressources ne sont disponibles qu'à l'achat, pas d'abonnement possible sur une ressource.
6. La version d'un système d'exploitation est restreinte au format "x.y".
7. Chaque système d'exploitation a été construit par un et un seul constructeur.
8. Chaque terminal a été construit par un et un seul constructeur.
9. On estime que le nom sur la carte bancaire est le même que le couple "nom, prénom" du titulaire de la carte.
10. Une application, ou une ressource ne sont développée (et uploadée sur le store) que par un seul editeur.
11. Pour des questions évidentes de sécurité, nous simplifirons ici le stockage des infos de la carte de crédit à uniquement son numéro de compte. (aucun code de vérification ne sera sauvegardé)
12. On estime qu'un éditeur est une société : un utilisateur ne peut pas également être déclaré comme éditeur. Pour ce faire, il devra passer par une société. 
13. Une application peut avoir un coût de téléchargement payant mais aussi nécessité un abonnement. 
14. On juge que tout client payant par carte bancaire dispose du montant nécessaire sur son compte bancaire, le paiement peut donc se faire.
15. Tout utilisateur inscrit sur le NimpStore possède au moins un terminal sur lequel il peut télécharger au moins une application.

- La notion de cadeau (offrir une application à quelqu'un d'autre) est, dans un premier temps, mise de coté. 
