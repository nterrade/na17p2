--- ***** Suppression du schéma ***** ---

DROP SCHEMA IF EXISTS projet CASCADE;
CREATE SCHEMA projet ;

--- ***** Changement du type de format de la date afin de l'avoir dans le format dd/mm/yyyy (et non plus yyyy/mm/dd) ***** ---

-- ALTER DATABASE "projetPerso" SET DateStyle =iso, dmy;

--- ***** Creation des tables ***** ---

------------------------------------------------------------
CREATE TABLE projet.editor (
    id INTEGER PRIMARY KEY,
    name VARCHAR UNIQUE NOT NULL,
    contact VARCHAR UNIQUE NOT NULL,
    url VARCHAR UNIQUE NOT NULL
);

------------------------------------------------------------
CREATE TABLE projet.users (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL, 
    first_name VARCHAR NOT NULL,
    mail VARCHAR UNIQUE NOT NULL, 
    password VARCHAR NOT NULL,
    birthdate DATE NOT NULL,
    admin_priviledge BOOLEAN NOT NULL,
    newsletter_subscription BOOLEAN NOT NULL
);

------------------------------------------------------------
CREATE TABLE projet.prepaid_card (
    serial_number VARCHAR(16) PRIMARY KEY,
    starting_amount FLOAT NOT NULL CHECK(starting_amount > 0),
    current_amount FLOAT NOT NULL CHECK(current_amount >= 0),
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    belong_to_user_id INTEGER NOT NULL,
    FOREIGN KEY (belong_to_user_id) REFERENCES projet.users(id),
    -- différentes contraintes de vérification sur les montants et sur le numéro de série
    CONSTRAINT chk_ammount CHECK (current_amount <= starting_amount),
    CONSTRAINT chk_date CHECK (start_date < end_date),
    CONSTRAINT chk_serial_number CHECK (LENGTH(serial_number) = 16)
);

------------------------------------------------------------
CREATE TABLE projet.credit_card (
    serial_number VARCHAR PRIMARY KEY,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    belong_to_user_id INTEGER NOT NULL,
    FOREIGN KEY (belong_to_user_id) REFERENCES projet.users(id),
    CONSTRAINT chk_date CHECK (start_date < end_date),
    -- contrainte permettant de vérifier que tous les numéros de série sont de longueur = 16
    CONSTRAINT chk_serial_number CHECK (LENGTH(serial_number) = 16)
);

------------------------------------------------------------
CREATE TABLE projet.device_type (
    type VARCHAR PRIMARY KEY
);

------------------------------------------------------------
CREATE TABLE projet.application (
    id INTEGER PRIMARY KEY,
    title VARCHAR NOT NULL,
    cost_of_download FLOAT CHECK(cost_of_download >= 0) NOT NULL,
    age_restrition INTEGER CHECK(age_restrition > 0),
    editor_id INTEGER NOT NULL,
    FOREIGN KEY (editor_id) REFERENCES projet.editor(id)
);

------------------------------------------------------------
CREATE TABLE projet.ressource (
    id INTEGER PRIMARY KEY,
    title VARCHAR NOT NULL,
    cost_of_download FLOAT CHECK(cost_of_download >= 0) NOT NULL,    
    age_restrition INTEGER CHECK(age_restrition > 0), 
    application_id INTEGER NOT NULL,
    editor_id INTEGER NOT NULL,
    FOREIGN KEY (editor_id) REFERENCES projet.editor(id),
    FOREIGN KEY (application_id) REFERENCES projet.application(id)
);

------------------------------------------------------------
-- side note :  - en passant par l'interface de PgAdmin, l'affichage des données ne se fait pas correctement pour cette table (texte en blanc)
--              - cependant les données sont bien présentes: on voit les lignes et lors d'un SELECT avec un WHERE, les résultats sont bien modifiés
--              - fonctionne parfaitement en passant par psql 
CREATE TABLE projet.constructor (
    name VARCHAR PRIMARY KEY, 
    country_origin VARCHAR NOT NULL
);

------------------------------------------------------------
CREATE TABLE projet.operating_system (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL,
    version FLOAT CHECK(version >= 0) NOT NULL,
    constructor_name VARCHAR NOT NULL,
    FOREIGN KEY (constructor_name) REFERENCES projet.constructor(name)
);

------------------------------------------------------------
CREATE TABLE projet.compatible (
    application_id INTEGER,
    OS_id INTEGER ,
    PRIMARY KEY (application_id, OS_id),
    FOREIGN KEY (application_id) REFERENCES projet.application(id),
    FOREIGN KEY (OS_id) REFERENCES projet.operating_system(id)
);

------------------------------------------------------------
CREATE TABLE projet.device (
    serial_number VARCHAR PRIMARY KEY,
    commercial_name VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    OS_id INTEGER NOT NULL,
    constructor_name VARCHAR NOT NULL,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (type) REFERENCES projet.device_type(type),
    FOREIGN KEY (constructor_name) REFERENCES projet.constructor(name),
    FOREIGN KEY (OS_id) REFERENCES projet.operating_system(id),
    FOREIGN KEY (user_id) REFERENCES projet.users(id) 
);

------------------------------------------------------------
CREATE TABLE projet.review (
    id INTEGER PRIMARY KEY,
    mark INTEGER CHECK (mark between 1 and 5) NOT NULL,
    title VARCHAR NOT NULL, 
    content TEXT CHECK (LENGTH(content) < 500) NOT NULL,
    application_id INTEGER NOT NULL,
    FOREIGN KEY (application_id) REFERENCES projet.application(id)
);

------------------------------------------------------------
-- pas de perspective d'évolution prévu pour cette donnée, donc on privilégie un type ENUM plutôt que la création d'une nouvelle table
DROP TYPE IF EXISTS PERIODICITY;
CREATE TYPE PERIODICITY AS ENUM ('monthly', 'quaterly', 'yearly');
CREATE TABLE projet.subscription (
    id INTEGER PRIMARY KEY, 
    application_id INTEGER NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    periodicity PERIODICITY NOT NULL,
    auto_debit BOOLEAN NOT NULL, 
    nb_month_debit INTEGER,
    cost FLOAT NOT NULL,
    FOREIGN KEY (application_id) REFERENCES projet.application(id),
    CONSTRAINT chk_date CHECK (start_date < end_date), 
    -- contrainte sur le renouvellement du paiement: 
    --      si le paiement est renouvelé automatiquement -> pas de nombre de mois avant la fin du prélèvement
    --      si le prélèvement n'est pas renouvelé automatiquement -> on doit avoir un nombre de mois de prélèvement
    CONSTRAINT chk_debit CHECK (
        (NOT auto_debit AND nb_month_debit IS NOT NULL)
        OR
        (auto_debit AND nb_month_debit IS NULL)
    )
);

------------------------------------------------------------
CREATE TABLE projet.download_app (
    user_id INTEGER, 
    application_id INTEGER,
    PRIMARY KEY (application_id, user_id),
    FOREIGN KEY (application_id) REFERENCES projet.application(id),
    FOREIGN KEY (user_id) REFERENCES projet.users(id)
);

------------------------------------------------------------
CREATE TABLE projet.download_ressource (
    user_id INTEGER, 
    ressource_id INTEGER,
    PRIMARY KEY (ressource_id, user_id),
    FOREIGN KEY (ressource_id) REFERENCES projet.ressource(id),
    FOREIGN KEY (user_id) REFERENCES projet.users(id)
);







--- ***** Création des vues ***** ---

-- vue permettant de voir le nombre de terminal pour chaque utilisateur 
DROP VIEW IF EXISTS projet.v_user_device CASCADE ;
CREATE VIEW projet.v_user_device AS (
	SELECT u.id, u.name, u.first_name ,count(d.user_id) AS nb_device
	FROM projet.device AS d
	RIGHT JOIN projet.users AS u
	ON d.user_id = u.id
	GROUP BY u.id, u.name, u.first_name
	ORDER BY u.id
);


-- vue permettant de voir de façon clair toutes les applications téléchargées par chaque utilisateur
DROP VIEW IF EXISTS projet.v_user_downloaded_app CASCADE;
CREATE VIEW projet.v_user_downloaded_app AS (
	select 
	u.id as user_id, u.name, u.first_name, da.application_id, a.title
	from 
	(projet.users as u
	left join projet.download_app as da
	on da.user_id = u.id )
	left join projet.application as a on a.id = da.application_id
	group by u.id, da.application_id, a.title
	order by u.id
);


-- vue permettant de voir de façon clair toutes les ressources téléchargées par chaque utilisateur
DROP VIEW IF EXISTS projet.v_user_downloaded_ressource CASCADE;
CREATE VIEW projet.v_user_downloaded_ressource AS (
	select 
	u.id as user_id, u.name, u.first_name, dr.ressource_id, r.title
	from 
	(projet.users as u
	left join projet.download_ressource as dr
	on dr.user_id = u.id )
	right join projet.ressource as r on r.id = dr.ressource_id
	group by u.id, dr.ressource_id, r.title
	order by u.id
);


-- vue permettant d'avoir le nombre de téléchargement de chaque application, ainsi que son profit, de la plus à la moins téléchargée
DROP VIEW IF EXISTS projet.v_nb_download_app CASCADE;
CREATE VIEW projet.v_nb_download_app AS (
	select application_id, a.title, count(application_id) as nb_download, a.cost_of_download, ((count(application_id)*a.cost_of_download)*0.7) as profit_after_commission
    from projet.download_app, projet.application as a
    where a.id = application_id
    group by download_app.application_id, a.title, a.cost_of_download
    order by nb_download DESC 
);


-- vue permettant d'avoir le nombre de téléchargement de chaque ressource, ainsi que son profit, de la plus à la moins téléchargée
DROP VIEW IF EXISTS projet.v_nb_download_ressource CASCADE;
CREATE VIEW projet.v_nb_download_ressource AS (
	select ressource_id, r.title, count(ressource_id) as nb_download 
	from projet.download_ressource, projet.ressource as r
	where r.id = ressource_id
	group by download_ressource.ressource_id, r.title
	order by nb_download DESC
);

-- vue permettant d'avoir directement le nom de toutes les applications développées par un éditeur
DROP VIEW IF EXISTS projet.v_editors_app CASCADE;
CREATE VIEW projet.v_editors_app AS (
	select e.id, e.name, a.title
	from (projet.editor as e
	right join projet.application as a
	on a.id = e.id)
	group by e.id, a.id
	order by e.id
);

-- vue permettant d'avoir directement le nom de toutes les ressources développées par un éditeur
DROP VIEW IF EXISTS projet.v_editors_ressource CASCADE;
CREATE VIEW projet.v_editors_ressource AS (
	select e.id, e.name, r.title
	from (projet.editor as e
	right join projet.ressource as r
	on r.id = e.id)
	group by e.id, r.id
	order by e.id
);




--- ***** Création de fonction et de trigger ***** ---

-- création de la fonction qui va vérifier le nombre de terminal de l'utilisateur en question
-- elle renvoie une erreur si l'utilisateur pour qui on essaye d'insérer un terminal possède déjà 5 terminals enregistrés
CREATE OR REPLACE FUNCTION projet.count_device() 
	RETURNS TRIGGER AS $BODY$ 
DECLARE
        row_count integer;
BEGIN 
	SELECT count(*) INTO row_count FROM projet.device AS d WHERE d.user_id = NEW.user_id;
	IF row_count >= 5 THEN
		RAISE EXCEPTION 'You have already reached the 5 device limit.';
	END IF;
	RETURN NEW; 
END; 
$BODY$ LANGUAGE plpgsql; 

-- création du trigger associé à la fonction précédente, il la déclanche avant chaque INSERT dans la table "device"
DROP TRIGGER IF EXISTS count_device ON projet.device;
CREATE TRIGGER count_device BEFORE INSERT ON projet.device
    FOR EACH ROW EXECUTE PROCEDURE projet.count_device();













--- ***** Insertion des données dans les tables ***** ---

--- Réinitialisation éventuelle des séquences ---

DROP SEQUENCE IF EXISTS projet.editor_id_seq;
DROP SEQUENCE IF EXISTS projet.users_id_seq;
DROP SEQUENCE IF EXISTS projet.application_id_seq;
DROP SEQUENCE IF EXISTS projet.ressource_id_seq;
DROP SEQUENCE IF EXISTS projet.operating_system_id_seq;
DROP SEQUENCE IF EXISTS projet.review_id_seq;
DROP SEQUENCE IF EXISTS projet.subscription_id_seq;

CREATE SEQUENCE projet.editor_id_seq START WITH 1;
CREATE SEQUENCE projet.users_id_seq START WITH 1;
CREATE SEQUENCE projet.application_id_seq START WITH 1;
CREATE SEQUENCE projet.ressource_id_seq START WITH 1;
CREATE SEQUENCE projet.operating_system_id_seq START WITH 1;
CREATE SEQUENCE projet.review_id_seq START WITH 1;
CREATE SEQUENCE projet.subscription_id_seq START WITH 1;


--- Insert into EDITOR ---

INSERT INTO projet.editor(id, name, contact, url) VALUES (nextval('projet.editor_id_seq'), 'Nintendo', 'nintendo@gmail.com', 'https://www.nintendo.com');
INSERT INTO projet.editor(id, name, contact, url) VALUES (nextval('projet.editor_id_seq'), 'Cultured Code & Co', 'culturedcode@gmail.com', 'https://www.culturedcode.com');
INSERT INTO projet.editor(id, name, contact, url) VALUES (nextval('projet.editor_id_seq'), 'Epic Games', 'epicgames@gmail.com', 'https://www.epic.com');
INSERT INTO projet.editor(id, name, contact, url) VALUES (nextval('projet.editor_id_seq'), 'Microsoft Corporation', 'microsoft@gmail.com', 'https://www.microsoft.com');
INSERT INTO projet.editor(id, name, contact, url) VALUES (nextval('projet.editor_id_seq'), 'Doist', 'doist@gmail.com', 'https://www.doist.com');
INSERT INTO projet.editor(id, name, contact, url) VALUES (nextval('projet.editor_id_seq'), 'SEGA', 'sega@gmail.com', 'https://www.sega.com');

--- Insert into USERS ---

insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Huckell', 'Olav', 'ohuckell0@a8.net', '7c7af2c116a44ad9d8e8975f08339681465e1db6', '1988-09-08', false, false);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Clench', 'Giulia', 'gclench1@cnn.com', '749090dc9fb0cdd535ba568c94d9afe6b320bc6d', '1978-08-05', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Kensington', 'Mendel', 'mkensington2@so-net.ne.jp', 'f52a99f1939eb6f4f3711ab1dced0f56ec0d76c7', '2014-02-04', false, false);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Probate', 'Katie', 'kprobate3@skype.com', '95d6cb9a24ae5ba0b5b287e99ea60f484f77c7ac', '2012-06-27', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Burton', 'Cristine', 'cburton4@mozilla.org', 'a5af3a5845757d761e1cb1fb72f47819d2fcc5da', '1988-01-31', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Tinto', 'Debor', 'dtinto5@whitehouse.gov', '84755507b959309a0c84403565afcfe98f181ac7', '1999-09-27', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Dafter', 'Arnoldo', 'adafter6@hp.com', '7162e596b540c9af8ca88ba5d966befbcee30f0d', '2010-02-03', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Bartelot', 'Thorny', 'tbartelot7@kickstarter.com', '1eb9f900e30b423bdc440b6ff9db5bc8dddd9208', '1972-01-29', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Whittam', 'Valerie', 'vwhittam8@goodreads.com', '28110c1e7100beee0e43eed4f634e96851e92a27', '1960-08-09', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Dutnall', 'Boy', 'bdutnall9@360.cn', '09effb8457ed12f96caf96a6d29f66f85f26a766', '1955-06-22', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Paulucci', 'Antonio', 'apauluccia@narod.ru', 'b1ab9d6056749c6475764fb4ba0c98bc002e5e24', '2003-11-01', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'McCarver', 'Orson', 'omccarverb@friendfeed.com', '4f06b12a592ac8a128351c908a71bd3bade6e1e1', '1958-04-30', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Shillum', 'Thain', 'tshillumc@purevolume.com', '731755466aeb5731b3be99975fc04f027180bfc1', '1978-04-03', false, false);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Le Clercq', 'Isidore', 'ileclercqd@hugedomains.com', '7f770aeb0136303995b110605adfda2c48291f94', '2006-05-04', false, false);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Griffith', 'Stormy', 'sgriffithe@nytimes.com', '6798487e2bdbb4902d9bd16405055f54731b98a8', '1974-09-15', false, true);
insert into projet.users (id, name, first_name, mail, password, birthdate, admin_priviledge, newsletter_subscription) values (nextval('projet.users_id_seq'), 'Terrade', 'Nathan', 'nathan.terrade@gmail.com', '9867487e2bdbb4902d9bd164055482f4731b98a8', '1998-06-12', true, true);

--- Insert into PREPAID_CARD ---

INSERT INTO projet.prepaid_card(serial_number, starting_amount, current_amount, start_date, end_date, belong_to_user_id) VALUES ('0010268795354576', 50.0, 50.0, '2019/06/12','2025/01/01', 2);
INSERT INTO projet.prepaid_card(serial_number, starting_amount, current_amount, start_date, end_date, belong_to_user_id) VALUES ('1234567891827260', 10.0, 3.01, '2019/12/25','2020/12/25', 9);
INSERT INTO projet.prepaid_card(serial_number, starting_amount, current_amount, start_date, end_date, belong_to_user_id) VALUES ('1658341676294683', 20.0, 20.0, '2020/01/05','2021/08/14', 6);
INSERT INTO projet.prepaid_card(serial_number, starting_amount, current_amount, start_date, end_date, belong_to_user_id) VALUES ('5689413296549523', 100.0, 62.45, '2020/04/30','2022/04/30', 12);

--- Insert into CREDIT_CARD ---

INSERT INTO projet.credit_card(serial_number, start_date, end_date, belong_to_user_id) VALUES ('4978744359181999', '2018/03/16', '2021/03/16', 16);
INSERT INTO projet.credit_card(serial_number, start_date, end_date, belong_to_user_id) VALUES ('9546738031967915', '2019/09/11', '2022/09/11', 8);
INSERT INTO projet.credit_card(serial_number, start_date, end_date, belong_to_user_id) VALUES ('4824817215216736', '2020/03/23', '2023/03/23', 15);
INSERT INTO projet.credit_card(serial_number, start_date, end_date, belong_to_user_id) VALUES ('4978643153054032', '2019/06/18', '2022/06/18', 1);
INSERT INTO projet.credit_card(serial_number, start_date, end_date, belong_to_user_id) VALUES ('4683152497620230', '2018/11/29', '2021/11/29', 5);


--- Insert into DEVICE_TYPE ---

INSERT INTO projet.device_type(type) VALUES ('smartphone');
INSERT INTO projet.device_type(type) VALUES ('tablet');
INSERT INTO projet.device_type(type) VALUES ('smartwatch');

--- Insert into APPLICATION ---

INSERT INTO projet.application(id, title, cost_of_download, age_restrition, editor_id) VALUES (nextval('projet.application_id_seq'), 'Mario Kart', 0.0, NULL, 1);
INSERT INTO projet.application(id, title, cost_of_download, age_restrition, editor_id) VALUES (nextval('projet.application_id_seq'), 'Things 3', 9.99, NULL, 2);
INSERT INTO projet.application(id, title, cost_of_download, age_restrition, editor_id) VALUES (nextval('projet.application_id_seq'), 'Fortnite', 0.0, NULL, 3);
INSERT INTO projet.application(id, title, cost_of_download, age_restrition, editor_id) VALUES (nextval('projet.application_id_seq'), 'Outlook', 0.0, NULL, 4);
INSERT INTO projet.application(id, title, cost_of_download, age_restrition, editor_id) VALUES (nextval('projet.application_id_seq'), 'To Doist', 3.99, NULL, 5);
INSERT INTO projet.application(id, title, cost_of_download, age_restrition, editor_id) VALUES (nextval('projet.application_id_seq'), 'Sonic', 0.0, NULL, 6);

--- Insert into RESSOURCE ---

INSERT INTO projet.ressource(id, title, cost_of_download, age_restrition, application_id, editor_id) VALUES (nextval('projet.ressource_id_seq'), 'DLC01', 0.0, NULL, 1, 1);
INSERT INTO projet.ressource(id, title, cost_of_download, age_restrition, application_id, editor_id) VALUES (nextval('projet.ressource_id_seq'), 'Exclusive Content', 2.99, 10, 1, 1);
INSERT INTO projet.ressource(id, title, cost_of_download, age_restrition, application_id, editor_id) VALUES (nextval('projet.ressource_id_seq'), 'Every Platform pack', 29.99, NULL, 2, 2);
INSERT INTO projet.ressource(id, title, cost_of_download, age_restrition, application_id, editor_id) VALUES (nextval('projet.ressource_id_seq'), 'Starting Pack', 9.99, 3, 3, 3);
INSERT INTO projet.ressource(id, title, cost_of_download, age_restrition, application_id, editor_id) VALUES (nextval('projet.ressource_id_seq'), 'Premium Pack', 9.99, NULL, 5, 5);

--- Insert into CONSTRUCTOR ---

INSERT INTO projet.constructor(name, country_origin) VALUES ('Apple', 'USA');
INSERT INTO projet.constructor(name, country_origin) VALUES ('Google', 'USA');
INSERT INTO projet.constructor(name, country_origin) VALUES ('Samsung', 'Corée du Sud');
INSERT INTO projet.constructor(name, country_origin) VALUES ('One Plus', 'Chine');
INSERT INTO projet.constructor(name, country_origin) VALUES ('Microsoft', 'USA');
INSERT INTO projet.constructor(name, country_origin) VALUES ('Huawei', 'Chine');

--- Insert into OPERATING_SYSTEM ---

INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'iOS', 13.5, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'iOS', 13.0, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'iOS', 12.0, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'iPadOS', 13.5, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'iPadOS', 13.0, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'iPadOS', 12.0, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'watchOS', 6.2, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'watchOS', 6.0, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'watchOS', 5.3, 'Apple');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'Android', 10.0, 'Google');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'Android', 9.0, 'Google');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'Android', 8.0, 'Google');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'Android', 7.0, 'Google');
INSERT INTO projet.operating_system(id, name, version, constructor_name) VALUES (nextval('projet.operating_system_id_seq'), 'Android', 6.0, 'Google');

--- Insert into COMPATIBLE ---

INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 1); -- insert pour Mario Kart
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 2);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 4);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 5);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 7);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 8);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 9);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 10);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 11);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (1, 12);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 1); -- insert pour Things 3
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 2);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 3);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 4);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 5);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 6);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 7);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 8);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (2, 9);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (3, 1); -- insert pour Fornite
INSERT INTO projet.compatible(application_id, OS_id) VALUES (3, 2);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (3, 4);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (3, 5);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (3, 10);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (3, 11);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (3, 12);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 1); -- insert pour Outlook
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 2);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 3);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 4);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 5);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 6);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 7);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 8);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 9);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 10);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 11);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 12);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 13);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (4, 14);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 1); -- insert pour To Doist
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 2);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 3);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 4);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 5);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 6);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 7);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 8);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (5, 9);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 1); -- insert pour Sonic
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 2);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 4);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 5);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 10);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 11);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 12);
INSERT INTO projet.compatible(application_id, OS_id) VALUES (6, 13);

--- Insert into DEVICE ---
-- iOS : 1-3
-- iPadOS : 4-6
-- watch OS : 7-9
-- Android : 10-14

INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('AAAA1111', 'iPhone 11 Pro Max', 'smartphone', 1, 'Apple', 16);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('AAAA2222', 'iPad Pro 2018 11p', 'tablet', 4, 'Apple', 16);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('AAAA3333', 'Apple Watch serie 4', 'smartwatch', 7, 'Apple', 16);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('AAAA4444', 'iPhone 8+', 'smartphone', 2, 'Apple', 16);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('AAAA5555', 'iPad Mini 4', 'tablet', 5, 'Apple', 16);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('AJDJH154', 'Galaxy Note 10', 'smartphone', 10, 'Samsung', 4);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('153DJHFD', 'Galaxy S10', 'smartphone', 10, 'Samsung', 1);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('HDZER264', 'Galaxy S8', 'smartphone', 11, 'Samsung', 2);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('JHFIUEZ2', 'Galaxy S6', 'smartphone', 12, 'Samsung', 3);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('DJS65379', 'Apple Watch serie 4', 'smartwatch', 8, 'Apple', 3);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('4581DSF7', 'One Plus 7', 'smartphone', 10, 'One Plus', 5);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('SDJFZ697', 'One Plus 6', 'smartphone', 10, 'One Plus', 6);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('565ZEF5S', 'Mate 20 Pro', 'smartphone', 10, 'Huawei', 7);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('2DS4F6ZE', 'Mate 30 Pro', 'smartphone', 10, 'Huawei', 8);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('DF54D47F', 'Mate 20', 'smartphone', 10, 'Huawei', 9);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('AIJF2566', 'iPhone 8', 'smartphone', 2, 'Apple', 10);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('DS5F45SD', 'iPhone 6S', 'smartphone', 2, 'Apple', 11);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('D5AZ4545', 'iPhone X', 'smartphone', 1, 'Apple', 12);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('DSF456F4', 'iPad Pro 2018 13p', 'tablet', 4, 'Apple', 12);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('KDJ686DF', 'iPhone XS', 'smartphone', 1, 'Apple', 13);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('IEZ5ERZ4', 'iPhone 7', 'smartphone', 2, 'Apple', 14);
INSERT INTO projet.device(serial_number, commercial_name, type, OS_id, constructor_name, user_id) VALUES ('EZIRZ56D', 'iPhone 7', 'smartphone', 2, 'Apple', 15);

--- Insert into REVIEW ---

INSERT INTO projet.review(id, mark, title, content, application_id) VALUES (nextval('projet.review_id_seq'), 5, 'Bonne application', 'Très bon jeu qui permet de passer du bon temps entre amis!', 1);
INSERT INTO projet.review(id, mark, title, content, application_id) VALUES (nextval('projet.review_id_seq'), 4, 'Application sympa', 'Bonne application qui permet de se détendre le soir', 1);
INSERT INTO projet.review(id, mark, title, content, application_id) VALUES (nextval('projet.review_id_seq'), 1, 'Sérieusement ?', 'Autant d''attente pour ça? Je suis extrêmement déçu.', 1);
INSERT INTO projet.review(id, mark, title, content, application_id) VALUES (nextval('projet.review_id_seq'), 5, 'Génial !!', 'Superbe application qui m''a permit de miex m''organiser dans mes travaux de tous les jours', 2);
INSERT INTO projet.review(id, mark, title, content, application_id) VALUES (nextval('projet.review_id_seq'), 4, 'Avis sur Fortnite', 'Bon jeu qui permet de se détendre, application sympa, dommage que l''on soit obligé de payer pour avancer', 3);

--- Insert into DOWNLOAD_APP ---

INSERT INTO projet.download_app(user_id, application_id) VALUES (1, 1); -- download pour Mario Kart
INSERT INTO projet.download_app(user_id, application_id) VALUES (2, 1);
INSERT INTO projet.download_app(user_id, application_id) VALUES (3, 1);
INSERT INTO projet.download_app(user_id, application_id) VALUES (6, 1);
INSERT INTO projet.download_app(user_id, application_id) VALUES (8, 1);
INSERT INTO projet.download_app(user_id, application_id) VALUES (11, 1);
INSERT INTO projet.download_app(user_id, application_id) VALUES (12, 1);
INSERT INTO projet.download_app(user_id, application_id) VALUES (15, 1);
INSERT INTO projet.download_app(user_id, application_id) VALUES (12, 2); -- download pour Things3
INSERT INTO projet.download_app(user_id, application_id) VALUES (13, 2);
INSERT INTO projet.download_app(user_id, application_id) VALUES (16, 2);
INSERT INTO projet.download_app(user_id, application_id) VALUES (16, 3); -- download pour Fortnite
INSERT INTO projet.download_app(user_id, application_id) VALUES (12, 3);
INSERT INTO projet.download_app(user_id, application_id) VALUES (8, 3);
INSERT INTO projet.download_app(user_id, application_id) VALUES (7, 3);
INSERT INTO projet.download_app(user_id, application_id) VALUES (11, 3);
INSERT INTO projet.download_app(user_id, application_id) VALUES (16, 4); -- download pour Outlook
INSERT INTO projet.download_app(user_id, application_id) VALUES (1, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (3, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (4, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (5, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (6, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (7, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (8, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (9, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (10, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (11, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (12, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (13, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (14, 4);
INSERT INTO projet.download_app(user_id, application_id) VALUES (1, 5); -- download pour To Doist
INSERT INTO projet.download_app(user_id, application_id) VALUES (2, 5);
INSERT INTO projet.download_app(user_id, application_id) VALUES (5, 5);
INSERT INTO projet.download_app(user_id, application_id) VALUES (8, 5);
INSERT INTO projet.download_app(user_id, application_id) VALUES (1, 6); -- download pour Sonic
INSERT INTO projet.download_app(user_id, application_id) VALUES (16, 6); 
INSERT INTO projet.download_app(user_id, application_id) VALUES (12, 6);
INSERT INTO projet.download_app(user_id, application_id) VALUES (8, 6);
INSERT INTO projet.download_app(user_id, application_id) VALUES (7, 6);
INSERT INTO projet.download_app(user_id, application_id) VALUES (11, 6);

--- Insert into DOWNLOAD_RESSOURCE ---

INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (1, 1); -- download pour 'DLC01' (mario kart)
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (2, 1);
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (1, 2); -- download pour 'Exclusive content' (mario kart)
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (2, 2);
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (3, 2);
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (16, 3); -- download pour 'Every Platform pack' (things 3)
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (12, 4); -- download pour 'Starting Pack' (fortnite)
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (8, 4);
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (7, 4);
INSERT INTO projet.download_ressource(user_id, ressource_id) VALUES (5, 5); -- download pour 'Premium Pack' (to doist)

--- Insert into SUBSCRIPTION ---

INSERT INTO projet.subscription(id, application_id, start_date, end_date, periodicity, auto_debit, nb_month_debit, cost) VALUES (nextval('projet.subscription_id_seq'), 2, '2020/01/06', '2021/01/06', 'monthly', TRUE, NULL, 4.99);
INSERT INTO projet.subscription(id, application_id, start_date, end_date, periodicity, auto_debit, nb_month_debit, cost) VALUES (nextval('projet.subscription_id_seq'), 2, '2020/01/06', '2021/01/06', 'yearly', FALSE, 12, 49.99);
INSERT INTO projet.subscription(id, application_id, start_date, end_date, periodicity, auto_debit, nb_month_debit, cost) VALUES (nextval('projet.subscription_id_seq'), 5, '2020/01/01', '2023/01/01', 'yearly', FALSE, 12, 59.99);
INSERT INTO projet.subscription(id, application_id, start_date, end_date, periodicity, auto_debit, nb_month_debit, cost) VALUES (nextval('projet.subscription_id_seq'), 5, '2020/01/01', '2023/01/01', 'monthly', true, NULL, 2.99);
INSERT INTO projet.subscription(id, application_id, start_date, end_date, periodicity, auto_debit, nb_month_debit, cost) VALUES (nextval('projet.subscription_id_seq'), 5, '2020/01/01', '2023/01/01', 'quaterly', false, 4, 10.99);
INSERT INTO projet.subscription(id, application_id, start_date, end_date, periodicity, auto_debit, nb_month_debit, cost) VALUES (nextval('projet.subscription_id_seq'), 5, '2020/01/01', '2023/01/01', 'quaterly', true, NULL, 2.99);